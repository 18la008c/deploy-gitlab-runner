# deploy-gitlab-runner
## How to use (with register)
1. remove existing gitlab-runner
2. make `.env` file such as `.env.sample`
3. `docker-compose up -d --build`
4. `docker exec -it docker-executor gitlab-runner register`
5. rewrite config.toml
    - add `/builds:/builds:rw` to use volume-mount in docker-compose
```
volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock:rw", "/builds:/builds:rw"]
```

## How to use (already registerd)
1. If already registered, `docker-compose up -d`
